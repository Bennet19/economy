package net.madgamble.economy;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.madgamble.core.api.common.translation.T;

public class JoinListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		EconomyManager manager = EconomyPlugin.getEconomyManager();
		if (!manager.hasAccount(p.getUniqueId(), p.getAddress().getHostString())) {
			manager.createNewAccount(p.getUniqueId(), p.getAddress().getHostString());
			T.send("economy.startmoney", p);
		}
	}
}
