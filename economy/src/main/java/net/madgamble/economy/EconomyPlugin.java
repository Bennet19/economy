package net.madgamble.economy;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.economy.Economy;

public class EconomyPlugin extends JavaPlugin {
	
	private static EconomyManager manager;
    private Economy econ = null;
	
	@Override
	public void onEnable() {
        if (!setupEconomy() ) {
            getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
		manager = new EconomyManager(this);
		this.getCommand("money").setExecutor(new MoneyCommand(this));
		
		Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
	}
	
    @Override
    public void onDisable() {
    	
    }
	
    public static EconomyManager getEconomyManager() {
    	return manager;
    }
    
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
    
	public Economy getEcon() {
		return econ;
	}

}
